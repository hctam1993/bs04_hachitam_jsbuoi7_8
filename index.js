var arr = [];
var arrFloat = [];
function themSo() {
  arr.push(document.getElementById("txt_so").value * 1);
  document.getElementById("txt_so").value = "";
  document.getElementById("showArr").innerText = arr;

  // tổng số dương
  var sum = 0;
  var countPositive = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      // phần tử > 0
      sum += arr[i]; // tổng
      countPositive++; // đếm sô dương
    }
  }
  document.getElementById(
    "result1"
  ).innerText = ` Tổng số dương trong mảng = ${sum}`;

  // đếm số dương
  document.getElementById(
    "result2"
  ).innerText = ` Có ${countPositive} số dương trong mảng`;

  minArray();
  minPositive();
  lastEvenNumber();
  firstPrimeNumber();
}
function minArray() {
  var min = arr[0];
  for (var i = 1; i < arr.length; i++) {
    if (arr[i] < min) {
      min = arr[i];
    }
  }
  document.getElementById(
    "result3"
  ).innerText = ` Số nhỏ nhất trong mảng = ${min}`;
}
function minPositive() {
  var min;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      min = arr[i]; // số dương đầu tiên tìm thấy gán = min
      for (var j = i + 1; j < arr.length; j++) {
        if (arr[j] > 0 && arr[j] < min) {
          min = arr[j];
        }
      }
      break;
    }
  }
  document.getElementById(
    "result4"
  ).innerText = ` Số dương nhỏ nhất trong mảng = ${min}`;
}
function lastEvenNumber() {
  var evenNumber;
  for (var i = arr.length - 1; i >= 0; i--) {
    // duyệt ngược mảng
    if (arr[i] % 2 == 0) {
      evenNumber = arr[i];
      break; // thoát vòng lặp
    }
  }
  document.getElementById(
    "result5"
  ).innerText = ` Số chẵn cuối cùng trong mảng = ${evenNumber}`;
}

function hoanDoi() {
  var viTriX = document.getElementById("txt_x").value * 1;
  var viTriY = document.getElementById("txt_y").value * 1;

  var t = arr[viTriX];
  arr[viTriX] = arr[viTriY];
  arr[viTriY] = t;

  document.getElementById("result6").innerText = arr;
  document.getElementById("showArr").innerText = arr;
}
function mangTangDan() {
  var mangTang = arr;
  mangTang.sort(function (a, b) {
    return a - b;
  });
  //   console.log(mangTang);
  document.getElementById("result7").innerText = mangTang;
}
function isPrimeNumber(x) {
  var isPrime = true;
  if (x < 2) return false;
  else if (x > 2)
    for (var i = 2; i <= Math.floor(x / 2); i++) {
      if (x % i == 0) {
        return false;
      }
    }
  return true;
}

function firstPrimeNumber() {
  for (var i = 0; i < arr.length; i++) {
    if (isPrimeNumber(arr[i]) == true) {
      document.getElementById(
        "result8"
      ).innerText = ` Số nguyên tố đầu tiên là: ${arr[i]}`;
      return;
    }
  }
  document.getElementById("result8").innerText = `-1`;
}
function themSoThuc() {
  arrFloat.push(document.getElementById("txt_so_thuc").value * 1.0);
  document.getElementById("txt_so_thuc").value = "";
  document.getElementById("showArrFloat").innerText = arrFloat;
}
function demSoNguyen() {
  var count = 0;
  for (var i = 0; i < arrFloat.length; i++) {
    if (Number.isInteger(arrFloat[i]) == true) count++;
  }
  document.getElementById("result9").innerText = ` Có ${count} số nguyên`;
}
function ssAmDuong() {
  var demDuong = 0;
  var demAm = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] < 0) demAm++;
    else if (arr[i] > 0) demDuong++;
  }
  if (demAm > demDuong) {
    document.getElementById("result10").innerText = ` Số âm > số dương`;
  } else if (demAm < demDuong) {
    document.getElementById("result10").innerText = ` Số dương > số âm`;
  } else {
    document.getElementById("result10").innerText = ` Số âm = số dương`;
  }
}
